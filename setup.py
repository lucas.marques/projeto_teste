from setuptools import setup

setup(
    name='mylibrary',
    version='0.1',
    description='My first Python library',
    author='Lucas and Pietro',
    packages=['myfiles/src'],
    install_requires=[
        'numpy>=1.0',
    ],
)
