"""Run code using mylibrary"""
#pylint:disable=import-error,wrong-import-position
import mylibrary
import numpy as np

result = mylibrary.add_numbers(2, 3)
print("2 + 3 = ", result)

result = mylibrary.multiply_numbers(2558, 374)
print("2558 * 374 = ", result)

arr1 = np.array([1, 2])
arr2= np.array([3, 4])
arrsum = mylibrary.add_arrays(arr1, arr2)

print(arrsum)
