"""math function"""
import numpy as np

def add_numbers(var1, var2):
    """function to add two numbers"""
    return var1 + var2

def multiply_numbers(var1, var2):
    """function to multiply two numbers"""
    return var1 * var2

def add_arrays(arr1, arr2):
    """function to add two arrays"""
    return np.add(arr1, arr2)
